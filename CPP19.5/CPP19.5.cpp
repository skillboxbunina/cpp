#include <iostream>

class Animal
{

public:

    Animal() {} //����� �� ����� ����������� � ��������� ���� ��������� ?

    virtual void Voice()
    {
        std::cout << "something is wrong: Class Animal" << std::endl;
    }

};

class Cat : public Animal
{

public:
    void Voice() override
    {
        std::cout << "may" << std::endl;
    }
};

class Dog : public Animal
{

public:
    void Voice() override
    {
        std::cout << "gaf" << std::endl;
    }
};

class Rodent : public Animal
{

public:
    void Voice() override
    {
        std::cout << "pi-pi" << std::endl;
    }
};

int main()
{
    const int len = 5;
    Animal* arr[len];
    arr[0] = new Cat();
    arr[1] = new Dog();
    arr[2] = new Rodent();
    arr[3] = new Cat();
    arr[4] = new Dog();

    for (int i = 0; i < len; i++)
    {
        arr[i]->Voice();
        delete arr[i]; //������� ������ 
    }
    
    return 0;


}